#ifndef CUSTOM_LIBS_LEDS_H_
#define CUSTOM_LIBS_LEDS_H_

#define LED_Red_IOdir       P1DIR
#define LED_Red_port        P1OUT
#define LED_Red_pin         BIT5

#define LED_Green_IOdir     P1DIR
#define LED_Green_port      P1OUT
#define LED_Green_pin       BIT6

#define LED_Blue_IOdir      P1DIR
#define LED_Blue_port       P1OUT
#define LED_Blue_pin        BIT7


#endif /* CUSTOM_LIBS_LEDS_H_ */
