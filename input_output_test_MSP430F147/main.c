#include <msp430.h> 
#include "switches.h"
#include "leds.h"

void port_init(void);
/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer

	port_init();

	for(;;)
	{
	    if(SW01_port & SW01_pin)
	    {
	        LED_Red_port    |= LED_Red_pin;
	        LED_Green_port  |= LED_Green_pin;
	        LED_Blue_port   |= LED_Blue_pin;
	    }
	    else if(!(SW01_port & SW01_pin))
	    {
            LED_Red_port    &= ~LED_Red_pin;
            LED_Green_port  &= ~LED_Green_pin;
            LED_Blue_port   &= ~LED_Blue_pin;
	    }
	}
}

void port_init(void)
{
    //switches
    SW01_IOdir  &= ~SW01_pin;
    SW02_IOdir  &= ~SW02_pin;
    SW03_IOdir  &= ~SW03_pin;
    SW04_IOdir  &= ~SW04_pin;
    SW05_IOdir  &= ~SW05_pin;
    SW06_IOdir  &= ~SW06_pin;
    SW07_IOdir  &= ~SW07_pin;
    SW08_IOdir  &= ~SW08_pin;
    SW09_IOdir  &= ~SW09_pin;
    SW10_IOdir  &= ~SW10_pin;
    SW11_IOdir  &= ~SW11_pin;
    SW12_IOdir  &= ~SW12_pin;
    SW13_IOdir  &= ~SW13_pin;
    SW14_IOdir  &= ~SW14_pin;
    SW15_IOdir  &= ~SW15_pin;
    SW16_IOdir  &= ~SW16_pin;
    SW17_IOdir  &= ~SW17_pin;
    SW18_IOdir  &= ~SW18_pin;
    SW19_IOdir  &= ~SW19_pin;
    SW20_IOdir  &= ~SW20_pin;
    SW21_IOdir  &= ~SW21_pin;
    SW22_IOdir  &= ~SW22_pin;

    //leds
    LED_Red_IOdir   |= LED_Red_pin;
    LED_Green_IOdir |= LED_Green_pin;
    LED_Blue_IOdir  |= LED_Blue_pin;
}
